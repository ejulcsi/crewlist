import { computed, observable } from 'mobx';

import PersonModel from '../models/personModel';

import data from '../data.json';

export class PersonStore {
  @observable persons = [];

  loadPersons() {
    this.persons = data.map(item => {
      return new PersonModel(item);
    });
  }

  addPerson(person) {
    this.persons.push(person);
  }

  removePerson(id) {
    if (window.confirm('Are you sure about it?')) {
      this.persons = this.persons.filter(item => item.id !== id);
    }
  }

  @computed
  get jsonText() {
    return JSON.stringify(this.persons.map(person => person.toJS()), null, 4);
  }
}

export default new PersonStore();
