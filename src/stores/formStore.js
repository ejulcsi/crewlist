import { computed, observable } from 'mobx';
import personStore from './personStore';
import PersonModel from '../models/personModel';
import FormModel from '../models/formModel';

export class FormStore {
  @observable fields = [];

  constructor() {
    this.fields.push(new FormModel('Name', true, 'string', 'name'));
    this.fields.push(new FormModel('Job title', true, 'string', 'job'));
    this.fields.push(new FormModel('Age', true, 'integer', 'age'));
    this.fields.push(new FormModel('Nickname', false, 'string', 'nick'));
    this.fields.push(new FormModel('Employee', true, 'boolean', 'employee'));
  }

  @computed
  get data() {
    const fieldValues = {};
    this.fields.forEach(field => {
      fieldValues[field.name] = field.value;
    });
    return fieldValues;
  }

  @computed
  get ok() {
    return this.fields.filter(item => !item.ok).length === 0;
  }

  addPerson() {
    if (!this.ok) {
      return;
    }

    personStore.addPerson(new PersonModel(this.data));
  }
}

export default new FormStore();
