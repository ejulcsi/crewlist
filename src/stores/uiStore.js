import { computed, observable } from 'mobx';

export class UiStore {
  @observable isOverlay = false;

  @computed
  get overlayState() {
    return this.isOverlay ? 'show' : '';
  }

  setOverlay(value) {
    this.isOverlay = value;
  }
}

export default new UiStore();
