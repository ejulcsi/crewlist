import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import personStore from './stores/personStore';
import uiStore from './stores/uiStore';
import formStore from './stores/formStore';

import './index.css';


personStore.loadPersons();

window._debug = {personStore, uiStore, formStore};

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

