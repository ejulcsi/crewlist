import React, {Component} from "react";
import {observer} from "mobx-react";

import PersonTable from "./components/personTable";
import Button from "./components/button";
import Overlay from "./components/overlay";
import DataDump from "./components/dataDump";

import uiStore from "./stores/uiStore";

import "./App.css";


@observer
class App extends Component {
  render() {
    return (
      <div className={["App", uiStore.overlayState].join(' ')}>
        <Button cta="Add person" action="add" onClick={() => uiStore.setOverlay(true)}/>
        <PersonTable />
        <Overlay />
        <DataDump />
      </div>
    );
  }
}

export default App;
