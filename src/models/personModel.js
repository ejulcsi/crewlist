import { observable } from 'mobx';
import { getUUID } from '../utils/utils';

export default class PersonModel {
  id;
  @observable name;
  @observable title;
  @observable nick;
  @observable employee;
  @observable age;

  constructor(person) {
    const age = parseInt(person.age, 10);

    this.id = getUUID();
    this.name = person.name;
    this.title = person.title;
    this.nick = person.nick;
    this.employee = person.employee;
    this.age = isNaN(age) ? null : age;
  }

  toJS() {
    return {
      name: this.name,
      title: this.title,
      age: this.age,
      nick: this.nick,
      employee: this.employee
    };
  }
}
