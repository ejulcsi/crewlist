import { observable, computed } from 'mobx';

export default class FormModel {
  @observable label;
  @observable required;
  @observable type;
  @observable name;
  @observable value;
  @observable set = false;

  constructor(label, required, type, name) {
    this.label = label;
    this.required = required;
    this.type = type;
    this.name = name;
    this.value = '';
  }

  setValue(fieldValue) {
    if (this.type === 'integer') {
      const value = parseInt(fieldValue, 10);
      this.value = isNaN(value) ? '' : value;
    } else {
      this.value = fieldValue;
    }
    this.set = true;
  }

  @computed
  get ok() {
    return !this.required || this.set;
  }
}
