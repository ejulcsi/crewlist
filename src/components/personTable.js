import React, { Component } from 'react';
import { observer } from 'mobx-react';

import PersonItem from './personItem';

import personStore from '../stores/personStore';

import './personTable.css';

@observer
export default class PersonTable extends Component {
  render() {
    return (
      <table className="persons">
        <thead className="persons__header">
          <tr>
            <th>
              Name<br />job title
            </th>
            <th>Age</th>
            <th>Nickname</th>
            <th>Employee</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {personStore.persons.map(person =>
            <PersonItem key={person.id} person={person} />
          )}
        </tbody>
      </table>
    );
  }
}
