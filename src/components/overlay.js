import React, { Component } from 'react';
import { observer } from 'mobx-react';

import Form from './form';

import './overlay.css';

@observer
class Overlay extends Component {
  render() {
    return (
      <div className="overlay">
        <Form />
      </div>
    );
  }
}

export default Overlay;
