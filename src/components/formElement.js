import React, { Component } from 'react';
import { observer } from 'mobx-react';

@observer
class FormElement extends Component {
  render() {
    const field = this.props.field;

    let input;
    if (field.type !== 'boolean') {
      input = (
        <div className={['form__element', field.ok ? '' : 'error'].join(' ')}>
          <input
            type="text"
            name={field.name}
            id={field.name}
            value={field.value}
            onChange={e => field.setValue(e.target.value)}
          />
          <label htmlFor={field.name}>
            {field.label}
          </label>
        </div>
      );
    }

    if (field.type === 'boolean') {
      input = (
        <div className="form__element-group">
          <div className={['form__element', field.ok ? '' : 'error'].join(' ')}>
            <input
              type="radio"
              name={field.name}
              id={field.name + '_yes'}
              checked={field.set && field.value}
              onChange={() => field.setValue(true)}
            />
            <label htmlFor={field.name + '_yes'}>Employee</label>
          </div>
          <div className={['form__element', field.ok ? '' : 'error'].join(' ')}>
            <input
              type="radio"
              name={field.name}
              id={field.name + '_no'}
              checked={field.set && !field.value}
              onChange={() => field.setValue(false)}
            />
            <label htmlFor={field.name + '_no'}>Not Employee</label>
          </div>
        </div>
      );
    }

    return (
      <div class="form__row">
        {input}
      </div>
    );
  }
}

export default FormElement;
