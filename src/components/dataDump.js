import React, { Component } from 'react';
import { observer } from 'mobx-react';

import personStore from '../stores/personStore';

import './dataDump.css';

@observer
class DataDump extends Component {
  render() {
    return (
      <div>
        <h1>Data dump</h1>
        <textarea value={personStore.jsonText} readOnly />
      </div>
    );
  }
}

export default DataDump;
