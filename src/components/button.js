import React, { Component } from 'react';

import './button.css';

class Button extends Component {
  render() {
    let onClick;
    if (!this.props.disabled) {
      onClick = e => {
        e.preventDefault();
        this.props.onClick();
      };
    } else {
      onClick = e => {
        e.preventDefault();
      };
    }

    return (
      <button
        className={[
          this.props.action,
          this.props.disabled ? 'disabled' : ''
        ].join(' ')}
        onClick={onClick}
      >
        {this.props.cta}
      </button>
    );
  }
}

export default Button;
