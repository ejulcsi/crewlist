import React, { Component } from 'react';
import { observer } from 'mobx-react';

import Button from './button';

import personStore from '../stores/personStore';

import './form.css';

@observer
class PersonItem extends Component {
  handleRemoving = () => {
    personStore.removePerson(this.props.person.id);
  };

  render() {
    const { person } = this.props;
    return (
      <tr>
        <td>
          {person.name}
          <br />
          {person.title}
        </td>
        <td>
          {person.age}
        </td>
        <td>
          {person.nick}
        </td>
        <td className={person.employee ? 'employee' : ''} />
        <td>
          <Button cta="Delete" action="delete" onClick={this.handleRemoving} />
        </td>
      </tr>
    );
  }
}

export default PersonItem;
