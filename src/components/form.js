import React, { Component } from 'react';
import { observer } from 'mobx-react';

import FormElement from './formElement';
import Button from './button';

import uiStore from '../stores/uiStore';
import formStore from '../stores/formStore';

import './form.css';

@observer
class Form extends Component {
  handleForm = () => {
    if (!formStore.ok) {
      return;
    }

    formStore.addPerson();

    uiStore.setOverlay(false);
  };

  render() {
    return (
      <form className="form">
        {formStore.fields.map(field => {
          return <FormElement field={field} key={field.name} />;
        })}
        <div className="form__buttons">
          <Button cta="Ok" disabled={!formStore.ok} onClick={this.handleForm} />
          <Button
            cta="Cancel"
            action="cancel"
            onClick={() => uiStore.setOverlay(false)}
          />
        </div>
      </form>
    );
  }
}

export default Form;
